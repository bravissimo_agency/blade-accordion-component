<?php

namespace Bravissimo\BladeAccordionComponent;

use Illuminate\Support\ServiceProvider;

class BladeAccordionComponentServiceProvider extends ServiceProvider {
    public function boot() {
        $this->loadViewsFrom(__DIR__, 'accordion');
    }
}
