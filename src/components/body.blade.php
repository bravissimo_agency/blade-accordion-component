<transition
    @enter="AccordionProvider.$slideDown"
    @leave="AccordionProvider.$slideUp"
>
    <div 
        {{ $attributes }}
        v-show="AccordionProvider.isOpen"
        v-bind:id="AccordionProvider.id"
        :aria-labelledby="AccordionProvider.headerId"
        role="region"
        v-bind:hidden="!AccordionProvider.defaultOpen"
        v-cloak
    >
        {!! $slot !!}
    </div>
</transition>
