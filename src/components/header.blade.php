<button 
    {{ $attributes }}
    type="button"
    v-bind:id="AccordionProvider.headerId"
    v-bind:aria-expanded="AccordionProvider.isOpen.toString()"
    v-bind:aria-controls="AccordionProvider.id"
    @click="AccordionProvider.$toggle"
>
    {!! $slot !!}
</button>