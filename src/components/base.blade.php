@props([
    'id' => substr(md5(mt_rand()), 0, 7),
    'defaultOpen' => null
])

<accordion-provider
    v-bind:id="'{{ $id }}'"
    :default-open="{{ $defaultOpen ?? 'false' }}"
    v-slot:default="AccordionProvider"
>
    <div 
        {{ $attributes }}
        v-bind:class="{ 'isOpen': AccordionProvider.isOpen }"
    >
        {!! $slot !!}
    </div>
</accordion-provider>